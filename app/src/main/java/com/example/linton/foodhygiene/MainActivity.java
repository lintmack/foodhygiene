package com.example.linton.foodhygiene;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 *  This is the Main Activity for the application.
 *  Contains the methods for getting user permissions for gps location, gets user location and holds
 *  the AsyncTask within the GetRating Class. The GetRating Class forms a connection to the
 *  FoodHygiene and parses the returned JSON data.
 *
 *  @author linton
 *  @version 1.0
 *  @since 28/03/2018
 */

public class MainActivity extends AppCompatActivity {

    // Variable declarations.
    EditText restSearch;
    ListView listView;
    public static ArrayList<Restaurant> restaurantArrayList = new ArrayList<>();
    public static RadioButton bNameSearch, pCodeSearch, gpsSearch;
    LocationManager locationManager;
    LocationListener locationListener;
    Double lat = 0.00, lng = 0.00;
    String latS = "", lngS = "";
    public static int indexVal;
    RestaurantAdapter adapter;

    @Override
    // Gets permission from the user to use their location and internet connection.
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_postcode_search);

        // Variables initiated.
        restSearch = (EditText) findViewById(R.id.restSearch);
        listView = (ListView) findViewById(R.id.listView);
        bNameSearch = (RadioButton) findViewById(R.id.bNameSearch);
        pCodeSearch = (RadioButton) findViewById(R.id.pCodeSearch);
        gpsSearch = (RadioButton) findViewById(R.id.gpsSearch);

        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            //fetch data
        } else {
            //display error
        }

        // Gets users location.
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.i("Location", location.toString());
                lng = location.getLongitude();
                lat = location.getLatitude();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
            }

            @Override
            public void onProviderEnabled(String s) {
            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };
        // Checks if the user has given the relevant permissions.
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 20, 0, locationListener);
        }

    }

    // A New Intent is created to pass data to a new activity.
    // The users location and the restaurantArrayList are added to a bundle that is added to the
    // intent. The user is then sent to the MapsActivity.
    public void viewResultsMap(View view){
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("userLat", lat);
        intent.putExtra("userLng", lng);
        Bundle bundle = new Bundle();
        bundle.putSerializable("bundle", restaurantArrayList);
        intent.putExtras(bundle);
        startActivity(intent);

    }

    // Radio buttons are used to change the value of int indexVal.
    // When another radio button is selected it clears any others that have been selected.
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.bNameSearch:
                if (checked)
                    pCodeSearch.setChecked(false);
                gpsSearch.setChecked(false);
                indexVal = 1;
                break;
            case R.id.pCodeSearch:
                if (checked)
                    bNameSearch.setChecked(false);
                gpsSearch.setChecked(false);
                indexVal = 2;
                break;
            case R.id.gpsSearch:
                if (checked)
                    lngS = lng.toString();
                latS = lat.toString();
                bNameSearch.setChecked(false);
                pCodeSearch.setChecked(false);
                indexVal = 3;
                break;
        }
    }

    public void getResults(View view) {
        // Checks if the restaurantArrayList is empty and clears it if its not == null.
        if (restaurantArrayList != null){
            restaurantArrayList.clear();
        }
        // New instance of the GetRating Class initiated.
        GetRating task = new GetRating();

        // If statement checks the value of the indexValue, which is set by the radio buttons.
        // Depending on the value it alters the URL to perform the relevant search.

        if (indexVal == 1) {
            task.execute("http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_name&name=" +
                    restSearch.getText().toString());
        } else if (indexVal == 2) {
            task.execute("http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_postcode&postcode=" +
                    restSearch.getText().toString());
        } else if (indexVal == 3) {
            task.execute("http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_loc&lat=" +
                    latS + "&long=" + lngS);
        }
    }

    // method gets all the recently added establishments.
    public void getRecentlyAdded(View view) {
        // clears any radio button selections.
        bNameSearch.setChecked(false);
        pCodeSearch.setChecked(false);
        gpsSearch.setChecked(false);
        //checks if the arrayList is populated and clears it if its not == null.
        if (restaurantArrayList != null){
            restaurantArrayList.clear();
        }
        // Creates a new instance of the GetRating Class and changes the URL.
        GetRating task = new GetRating();
        task.execute("http://sandbox.kriswelsh.com/hygieneapi/hygiene.php?op=s_recent");
    }

    // GetRating Class uses the AsyncTask to run background tasks outside of the main thread.
    public class GetRating extends AsyncTask<String, Void, String> {

        @Override
        // Creates a connection.
        protected String doInBackground(String... urls) {
            String result = "";
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();
                // performs the loop, while there is data to be read.
                while (data != -1) {
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }
                // returns the result.
                return result;
            //catches exceptions.
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        // The returned result is passed as a parameter to the onPostExecuteMethod.
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                // A new JSON ArrayList and JSON Object are created.
                JSONArray jsonArray = new JSONArray(result);
                JSONObject jsonObject;
                for (int n = 0; n < jsonArray.length(); n++) {
                    jsonObject = jsonArray.getJSONObject(n);
                    //If the user has selected search by GPS then all parameters for the object,
                    //including distanceKM are required. Restaurant object is created from th JSON.
                    //
                    if (indexVal == 3) {
                        Restaurant RestaurantObj = new Restaurant(
                                (jsonObject.getString("BusinessName")),
                                (jsonObject.getString("AddressLine1")),
                                (jsonObject.getString("AddressLine2")),
                                (jsonObject.getString("AddressLine3")),
                                (jsonObject.getString("PostCode")),
                                (jsonObject.getString("RatingValue")),
                                (jsonObject.getString("RatingDate")),
                                (jsonObject.getString("Longitude")),
                                (jsonObject.getString("Latitude")),
                                (jsonObject.getString("DistanceKM")));
                        restaurantArrayList.add(RestaurantObj);
                    } else {
                        // All other searches do not require distanceKM variable.
                        Restaurant RestaurantObj = new Restaurant((jsonObject.getString("BusinessName")),
                                (jsonObject.getString("AddressLine1")),
                                (jsonObject.getString("AddressLine2")),
                                (jsonObject.getString("AddressLine3")),
                                (jsonObject.getString("PostCode")),
                                (jsonObject.getString("RatingValue")),
                                (jsonObject.getString("RatingDate")),
                                (jsonObject.getString("Longitude")),
                                (jsonObject.getString("Latitude")));
                        restaurantArrayList.add(RestaurantObj);

                    }
                }
                //Uses the RestaurantAdapter to create a custom List view from the restaurant objects
                // stored in the ArrayList.
                adapter = new RestaurantAdapter(MainActivity.this, restaurantArrayList);
                ListView listView = (ListView) findViewById(R.id.listView);
                listView.setAdapter(adapter);
                //JSON Exceptions are caught.
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // Result is logged in the console.
            Log.i("Result", result);
        }
    }
}
