package com.example.linton.foodhygiene;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * MapsActivity allows the user to view returned restaurant listings on the map.
 * They are displayed as individual pins, which when clicked display business name and hyg rating.
 * camera is set to automatically focus on the users location.
 *
 * @author linton
 * @version 1.0
 * @since 21/03/2018
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    //Variables declared.

    private GoogleMap mMap;
    ArrayList<Restaurant> restaurantArrayList = new ArrayList<>();
    double userLat, userLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Bundle passed from the Main Activity (PostCodeSearch is parsed.
        Bundle bundle = getIntent().getExtras();
        // Contents of bundle is deserialized and set to a new Arraylist.
        restaurantArrayList = (ArrayList<Restaurant>) bundle.getSerializable("bundle");

        // userLat & userLng variables initialized and set to values from the bundle.
        userLat = getIntent().getDoubleExtra("userLat", 0.0);
        userLng = getIntent().getDoubleExtra("userLng", 0.0);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //for loop used to loop through values in restaurantArrayList and create an individual
        //restaurant object.
        for (Restaurant restaurant : restaurantArrayList) {
            //BusinessName, Lat and Lng for listings is logged to console to check values are being
            // retrieved.
            Log.d("MapsActivity", "onMapReady: BusinessName: " + restaurant.getBusinessName());
            Log.d("MapsActivity", "onMapReady: Lat:" + restaurant.getLatitude());
            Log.d("MapsActivity", "onMapReady: Long:" + restaurant.getLongitude());

            //New random object instantiated.
            Random random = new Random();

            // New double values are declared and used to generate random numbers.
            double r1 = 0.0005 * random.nextDouble();
            double r2 = 0.0005 * random.nextDouble();

            // LatLng defined for each business listing. Random number is added to the double value
            // to moe each result marginally
            // on the map. Implemented to counter pin clustering.
            LatLng l = new LatLng(Double.parseDouble(restaurant.getLatitude()) + r1,
                    Double.parseDouble(restaurant.getLongitude()) + r2);

            // Markers added to the map for each business.
            // Business Name is set in the title. Snippet is set to the Hygiene Rating.
            // Icon sets a custom amp marker image.
            mMap.addMarker(new MarkerOptions().position(l).title(restaurant.getBusinessName())
                    .snippet("Hygiene Rating: " + restaurant.getRatingValue())
                    .icon(BitmapDescriptorFactory.fromResource(restaurant.getHygRatingMapDrawable())));
        }

        // New LatLng declared for user location.
        LatLng firstMarker = new LatLng(userLat, userLng);
        // camera set to zoom in on the users location.
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(firstMarker, 16));

    }
}

